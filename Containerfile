FROM registry.conexo.mx/kaxtli/debian:bookworm-slim

# This container acts as mailbox server:
#	- Manage and storage the mailbox
#	- Manage users
#	- Provide IMAP and SMTP submission services to users.
#	- recive new mail trough LMTP from postfix

# The design is to be a instance decoupled from MTA
#	- MTA do not need to manage users
#	- MTA do not need to store any mail message
#	- MTA interact only by LMTP on port 24


# Release information:
#	- Use MailDir because dovecot change to each user priveleges to write mail.
#	  so, dovecot can't write to /var/mail/	
# Limitations:
#	- Can't receive messenges to 'root' user.
#	- uses self signed certificate

# NOTE: use '-h domain.com' in 'podman run' to configure the domain name

# Usar en postfix:
# mailbox_transport = lmtp:localhost
# mailbox_transport = lmtp:10.88.0.20


# IMPORTANT: 
# by default submition_relay_host is set to 'localhost'
# in the instance, change this with the IP/FQDN of the MTA
# sed -i 's/submission_relay_host =.*/submission_relay_host = 10.88.0.22/' \
#    /etc/dovecot/conf.d/20-submission.conf


# Test:
#	doveadm auth login oscar
#	doveadm auth login oscar@aula.red

ENV DEBIAN_FRONTEND=noninteractive
ENV TERM=linux

# delivery
# EXPOSE 24 # only to a MTA

# Retrive
EXPOSE 143 
	# IMAP   STARTTLS
EXPOSE 993 
	# IMAPS  TLS
# submit
EXPOSE 587 
	# SMTP submission	STARTTLS
EXPOSE 465 
	# SMTPS submission	TLS

RUN echo "deb http://deb.debian.org/debian/ bookworm main contrib non-free" \
 >  /etc/apt/sources.list


RUN apt update \
 && apt install -y \
	dovecot-imapd \
	dovecot-submissiond \
	dovecot-lmtpd \
	dovecot-sieve \
	dovecot-ldap \
	bsd-mailx \	
 && echo log_path = /dev/stdout \
 >>     /etc/dovecot/conf.d/10-logging.conf  \

# doe #RUN doveconf 'auth_username_format = %Ln' # no f
# doveconf is only for queries, can't set variable values.

 # https://doc.dovecot.org/admin_manual/system_users_used_by_dovecot/
 # Remove the @domain part from username in LMTP to match users in /etc/passwd
 && echo 'auth_username_format = %Ln' \
    >> /etc/dovecot/conf.d/10-auth.conf \

 # New mail is received from MTA by LMTP port 24 # DO NOT EXPOSE to Internet
 && sed -i 's/service lmtp {/service lmtp { \n  inet_listener lmtp {\n    address = 0.0.0.0 \n    port = 24\n  }/' \
   /etc/dovecot/conf.d/10-master.conf  \

 # dovecot-imtpd do not have write permissions in /var/mail/
 && sed -i 's@^mail_location = .*@mail_location = maildir:~/Maildir@' \
    /etc/dovecot/conf.d/10-mail.conf \

 # IMPORTANT: Change this with the IP/FQDN of the MTA in the instance
 && sed -i 's/#submission_relay_host =.*/submission_relay_host = localhost/' \
    /etc/dovecot/conf.d/20-submission.conf \

 # enable 465 IMAPS
 && echo " \
    service submission-login {\n \
      inet_listener submissions {\n \
         port = 465\n \
         ssl = yes\n \
      }\n \
    }" >> /etc/dovecot/conf.d/10-master.conf
 


# Debug utilities
RUN apt update \
 && apt install -y nano procps s-nail


#CMD ["/usr/sbin/dovecot","-F"]
CMD ["dovecot","-F"]


# -----
#  https://wiki.dovecot.org/HowTo/PostfixDovecotLMTP
# plugins 
#protocol lmtp {
#  postmaster_address = postmaster@domainname   # required
#  mail_plugins = quota sieve
#}


# podman run --replace -d -h aula.red --name dovecot registry.conexo.mx/kaxtli/dovecot 
